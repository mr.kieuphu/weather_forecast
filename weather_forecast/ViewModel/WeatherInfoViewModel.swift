//
//  WeatherInfoViewModel.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import UIKit

class WeatherInfoViewModel: NSObject, WeatherInfoViewModelType {
    var date: String = ""
    var averageTemperature: String = ""
    var pressure: String = ""
    var humidity: String = ""
    var summary: String = ""
    
    var weatherInfo: String {
        return "Date: \(date)\nAverage Temperature: \(averageTemperature)\nPressure: \(pressure)\nHumidity: \(humidity)\nDescription: \(summary)"
    }
    
    init(weather: WeatherInfo) {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE, dd MMM yyyy"
        self.date = formatter.string(from: Date(timeIntervalSince1970: weather.date))
        self.averageTemperature = String(format: "%0.2f°C", weather.averageTemp)
        self.pressure = "\(weather.pressure)"
        self.humidity = "\(weather.humidity)%"
        self.summary = weather.description
    }
}
