//
//  WeatherInfoViewModel.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import UIKit

protocol WeatherInfoViewModelType {
    var date: String { get set }
    var averageTemperature: String { get set }
    var pressure: String { get set }
    var humidity: String { get set }
    var summary: String { get set }
    var weatherInfo: String { get }
}
