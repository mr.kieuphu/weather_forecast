//
//  WeatherForceCast.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import Foundation

public struct WeatherForceCast: BaseResponse {
    var code: String = ""
    var list: [WeatherInfo]?
    var count: Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "cod"
        case list = "list"
        case count = "cnt"
    }
}
