//
//  BaseResponse.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import Foundation

protocol BaseResponse: Codable {
    var code: String { get set }
}
