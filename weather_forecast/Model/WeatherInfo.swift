//
//  WeatherInfo.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import Foundation

struct WeatherInfo: Codable {
    var date: Double = 0
    var temp = Temp()
    var pressure: Int = 0
    var humidity: Double = 0
    var weather = [Weather]()
    
    var averageTemp: Double {
        return (temp.min + temp.max)/2
    }
    
    var description: String {
        return weather.first?.description ?? ""
    }
    
    enum CodingKeys: String, CodingKey {
        case date = "dt"
        case temp = "temp"
        case pressure = "pressure"
        case humidity = "humidity"
        case weather = "weather"
    }
}

struct Weather: Codable {
    var id: Int = 0
    var main: String = ""
    var description: String = ""
    var icon: String = ""
}

struct Temp: Codable {
    var min: Double = 0
    var max: Double = 0
}

