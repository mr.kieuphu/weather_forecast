//
//  WeatherInfoRequest.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import Foundation

public struct WeatherInfoRequest: Codable {
    var city: String = ""
    var count: Int = 0
    var appId: String = ""
    var units: String = ""
    
    enum CodingKeys: String, CodingKey {
        case city = "q"
        case count = "cnt"
        case appId = "appid"
        case units = "units"
    }
}
