
import Foundation
extension Encodable {
    func encoded() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func toJSON(_ option: JSONSerialization.ReadingOptions = .allowFragments) -> [String: Any] {
        let data = try? encoded()
        guard let json = (try? JSONSerialization.jsonObject(with: data ?? Data(), options: .allowFragments)) as? [String: Any] else {            
            return [:]
        }
        return json
    }
}

extension Data {
    func decoded<T: Decodable>() throws -> T {
        return try JSONDecoder().decode(T.self, from: self)
    }
}
