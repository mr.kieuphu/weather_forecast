//
//  WeatherForeCastViewModel.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import UIKit

protocol WeatherForeCastViewModelType {
    typealias Handler = ((_ state: WeatherForeCastViewController.State) -> Void)?
    func getWeatherForeCast(_ city: String, completion: Handler)

    var items: [WeatherInfoViewModelType] { get set }
}

class WeatherForeCastViewModel: WeatherForeCastViewModelType {
    var weatherForeCastService: WeatherForeCastServiceType?
    
    var items = [WeatherInfoViewModelType]()
    let appId = "60c6fbeb4b93ac653c492ba806fc346d"
    let units = "metric"
    
    init(weatherForeCastService: WeatherForeCastServiceType) {
        self.weatherForeCastService = weatherForeCastService
    }
    
    private lazy var fetchWeatherForeCastDebounce = debounce(interval: 300, queue: DispatchQueue.main, action: { [weak self] (city: String, completion: Handler) in
        guard let strongSelf = self else { return }
        strongSelf.fetchWeatherForeCast(city, completion: completion)
    })
    
    func getWeatherForeCast(_ city: String, completion: Handler) {
        if self.validCity(city) {
            fetchWeatherForeCastDebounce((city, completion))
        } else {
            self.items.removeAll()
            completion?(.displayWeatherInfo)
        }
    }
    
    private func fetchWeatherForeCast(_ city: String, completion: Handler) {
        self.weatherForeCastService?.fetchWeatherForeCast(WeatherInfoRequest(city: city, count: 7, appId: appId, units: units)) { [weak self] (result) in
            guard let strongSelf = self else { return }
            switch result {
            case let .success(data):
                var items = [WeatherInfoViewModelType]()
                if let list = data.list {
                    for weather in list {
                        items.append(WeatherInfoViewModel(weather: weather))
                    }
                    strongSelf.items = items
                } else {
                    strongSelf.items.removeAll()
                }
                completion?(.displayWeatherInfo)
            case let .failure(error):
                completion?(.showPopup(error.message))
            }
        }
    }
    
    func validCity(_ city: String) -> Bool {
        return city.count >= 3
    }
}

extension WeatherForeCastViewModel {
    typealias Debounce<T> = (_ : T) -> Void

    func debounce<T>(interval: Int, queue: DispatchQueue, action: @escaping Debounce<T>) -> Debounce<T> {
        var lastFireTime = DispatchTime.now()
        let dispatchDelay = DispatchTimeInterval.milliseconds(interval)

        return { param in
            lastFireTime = DispatchTime.now()
            let dispatchTime: DispatchTime = DispatchTime.now() + dispatchDelay

            queue.asyncAfter(deadline: dispatchTime) {
                let when: DispatchTime = lastFireTime + dispatchDelay
                let now = DispatchTime.now()

                if now.rawValue >= when.rawValue {
                    action(param)
                }
            }
        }
    }
}
