//
//  WeatherInfoTableCell.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import UIKit
import SnapKit

class WeatherInfoTableCell: UITableViewCell {
    
    let infoLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.configView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configView() {
        self.backgroundColor = .clear
        self.infoLabel.font = .preferredFont(forTextStyle: .body)
        self.infoLabel.adjustsFontForContentSizeCategory = true
        self.infoLabel.textColor = .black
        self.infoLabel.isAccessibilityElement = true
        self.infoLabel.numberOfLines = 0
        self.addSubview(self.infoLabel)
        
        self.setConstraintLayout()
    }
    
    private func setConstraintLayout() {
        self.infoLabel.snp.makeConstraints { [weak self] (maker) in
            guard let strongSelf = self else { return }
            maker.edges.equalTo(strongSelf).inset(UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16))
        }
    }
    
    func updateUI(_ model: WeatherInfoViewModelType) {
        self.infoLabel.text = model.weatherInfo
    }
}
