//
//  WeatherForeCastViewController.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import UIKit

class WeatherForeCastViewController: UIViewController {
    
    let tableView = UITableView()
    let searchController = UISearchController(searchResultsController: nil)
    private let viewModel: WeatherForeCastViewModelType
    
    lazy var handleStateCompletion: (State) -> Void = { [weak self] state in
        guard let strongSelf = self else { return }
        strongSelf.handleState(state)
    }
    
    init(viewModel: WeatherForeCastViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configView()
    }
    
    private func configView() {
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.isOpaque = true
        self.extendedLayoutIncludesOpaqueBars = true
        self.title = "Weather Forecast"
        self.view.backgroundColor = .white
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Enter City"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        self.navigationItem.searchController = searchController
        
        self.tableView.estimatedRowHeight = 180
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .singleLine
        self.tableView.allowsSelection = false
        self.tableView.backgroundColor = .white
        self.tableView.register(WeatherInfoTableCell.self, forCellReuseIdentifier: "cell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.view.addSubview(self.tableView)
        
        self.setConstraintLayout()
    }
    
    private func setConstraintLayout() {
        self.tableView.snp.makeConstraints { [weak self] (maker) in
            guard let strongSelf = self else { return }
            maker.edges.equalTo(strongSelf.view)
        }
    }
    
    private func handleState(_ state: State) {
        switch state {
        case .displayWeatherInfo:
            self.tableView.reloadData()
        case let .showPopup(message):
            self.showAlertPopup(message)
        }
    }
    
    private func showAlertPopup(_ message: String) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension WeatherForeCastViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? WeatherInfoTableCell {
            cell.updateUI(self.viewModel.items[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
}

extension WeatherForeCastViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !(searchController.searchBar.text?.isEmpty ?? false) {
            self.searchController.searchBar.resignFirstResponder()
        }
    }
}

extension WeatherForeCastViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        self.viewModel.getWeatherForeCast(searchController.searchBar.text ?? "", completion: handleStateCompletion)
    }
}

extension WeatherForeCastViewController {
    enum State {
        case displayWeatherInfo
        case showPopup(_ message: String)
    }
}
