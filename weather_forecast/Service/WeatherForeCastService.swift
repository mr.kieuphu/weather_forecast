//
//  WeatherForeCastService.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import UIKit
import Alamofire

public protocol WeatherForeCastServiceType {
    typealias WeatherForeCastRestAPICompletion<T> = (ResultResponse<T, ErrorType>) -> Void
    func fetchWeatherForeCast(_ param: WeatherInfoRequest, completion: @escaping WeatherForeCastRestAPICompletion<WeatherForceCast>)
}

public class WeatherForeCastService: WeatherForeCastServiceType {
    public func fetchWeatherForeCast(_ param: WeatherInfoRequest, completion: @escaping WeatherForeCastRestAPICompletion<WeatherForceCast>) {
        let target = WeatherForceCastTarget(parameters: param.toJSON())
        APIRequest.request(target, completion: completion)
    }
}

struct WeatherForceCastTarget: APIRequestTarget {
    var baseUrl: String { return  "https://api.openweathermap.org/data/2.5/forecast/daily"}
    var method: HTTPMethod { return .get}
    var parameters: [String : Any]
    var encoding: ParameterEncoding { return URLEncoding.queryString}
    
    init(parameters: [String: Any]) {
        self.parameters = parameters
    }
}
