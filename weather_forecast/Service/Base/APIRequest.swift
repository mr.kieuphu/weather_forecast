//
//  APIRequest.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import UIKit
import Alamofire

class APIRequest: NSObject {
    typealias RestAPICompletion<T: Decodable> = (ResultResponse<T, ErrorType>) -> Void
    static let sessionManager: Session = {
        let configuration = URLSessionConfiguration.af.default
        configuration.requestCachePolicy = .returnCacheDataElseLoad
        return Session(configuration: configuration)
    }()
    
    static func request<T: BaseResponse>(_ target: APIRequestTarget, completion: @escaping RestAPICompletion<T>) {
        self.sessionManager.request(target.baseUrl, method: target.method, parameters: target.parameters, encoding: target.encoding, headers: target.headers)
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    if let data = response.data {
                        do {
                            completion(ResultResponse.success(try data.decoded()))
                        } catch {
                            completion(ResultResponse.failure(ErrorType(code: -1001, message: "Can not pase data")))
                        }
                    }
                case let .failure(error):
                    completion(ResultResponse.failure(ErrorType(code: error.responseCode ?? -1, message: error.errorDescription ?? "")))
                }
            }
    }
}

protocol APIRequestTarget {
    var baseUrl: String { get }
    var method: HTTPMethod { get }
    var parameters: [String: Any] { get }
    var encoding: ParameterEncoding { get }
    var headers: HTTPHeaders { get }
}

extension APIRequestTarget {
    var headers: HTTPHeaders { return ["Accept": "application/json"]  }
}
