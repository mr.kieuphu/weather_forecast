//
//  APIResponse.swift
//  weather_forecast
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import UIKit

public enum ResultResponse<Value, ErrorType: Error> {
    case success(Value)
    case failure(ErrorType)
}

public struct ErrorType: Error {
    public var code: Int = 0
    public var message: String = ""
    
    public static func == (lhs: ErrorType, rhs: ErrorType) -> Bool {
        return lhs.code == rhs.code
    }
    
    public init(code: Int, message: String) {
        self.code = code
        self.message = message
    }
}
