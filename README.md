# Weather Forecast
An iOS app to retrieve weather information based on their searching criteria and render the searched results on dashboard screen

# The software development principles, patterns & practices
- Use MVVM Pattern
- Apply protocol oriented programing

# Folder structure
- Service: Handle api request, api response, cache response, define target for every api
- Model: Include structs specify Models
- View: Include class specify UI. Ex: ViewController, Table view Cell,...
- ViewModel: Handle data logic from model, business logic of View

# Frameworks/Libraries
- Alamofire: Apply for networking hanlder
- Snapkit: Use to layout UI
- Codable: Use to defile model. Easy to map object from json

# Checklist has done 
- The application is a simple iOS application which is written by Swift.
- The application is able to retrieve the weather information from OpenWeatherMaps API.
- The application is able to allow user to input the searching term.
- The application is able to proceed searching with a condition of the search term length must be
from 3 characters or above.
- The application is able to render the searched results as a list of weather items.
- The application is able to support caching mechanism so as to prevent the app from
generating a bunch of API requests.
- The application is able to handle failures.
- The application is able to support the disability to scale large text for who can't see the
text clearly.
- The application is able to support the disability to read out the text using VoiceOver
controls.

# How to run project
1. Clone project to your folder 
2. Download Cocapods if not: https://cocoapods.org/
3. Open terminal, cd to project folder and run: 
```pod install```
4. Open file ```weather_forecast.xcworkspace``` and run project.
