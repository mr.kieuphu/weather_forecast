//
//  WeatherInfoTests.swift
//  weather_forecastTests
//
//  Created by Kieu Minh Phu on 6/30/21.
//

import XCTest
@testable import weather_forecast

class WeatherInfoTests: XCTestCase {

    var sut: WeatherInfo!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = WeatherInfo()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testValidAverageTemp() throws {
        sut.temp.min = 20
        sut.temp.max = 39
        
        let averageTemp = sut.averageTemp
        
        XCTAssertEqual(averageTemp, 29.5, "Average temp is correct")
    }

}
