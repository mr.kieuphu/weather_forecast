//
//  weather_forecastTests.swift
//  weather_forecastTests
//
//  Created by Kieu Minh Phu on 6/29/21.
//

import XCTest
@testable import weather_forecast

class WeatherForeCastAPITests: XCTestCase {
    var sut: WeatherForeCastService!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = WeatherForeCastService()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testValidWeatherForeCastRequestStatusCode200() throws {
        let promise = expectation(description: "Status code: 200")
        let request = WeatherInfoRequest(city: "saigon", count: 7, appId: "60c6fbeb4b93ac653c492ba806fc346d", units: "metric")
        sut.fetchWeatherForeCast(request) { (result) in
            switch result {
            case .success:
                promise.fulfill()
            case let .failure(error):
                XCTFail(error.message)
            }
        }
        wait(for: [promise], timeout: 5)
    }
    
    func testCityNotFound() throws {
        let promise = expectation(description: "City not found")
        let request = WeatherInfoRequest(city: "sa", count: 7, appId: "60c6fbeb4b93ac653c492ba806fc346d", units: "metric")
        sut.fetchWeatherForeCast(request) { (result) in
            switch result {
            case let .success(data):
                if data.code == "404" {
                    promise.fulfill()
                } else {
                    XCTFail("City found")
                }
            case let .failure(error):
                XCTFail(error.message)
            }
        }
        wait(for: [promise], timeout: 5)
    }
}
