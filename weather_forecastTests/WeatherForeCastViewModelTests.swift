//
//  WeatherForeCastViewModelTests.swift
//  weather_forecastTests
//
//  Created by Kieu Minh Phu on 6/30/21.
//

import XCTest
@testable import weather_forecast

class WeatherForeCastViewModelTests: XCTestCase {
    
    var sut: WeatherForeCastViewModel!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = WeatherForeCastViewModel(weatherForeCastService: WeatherForeCastService())
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testNotFetchWeatherForeCast() throws {
        let city = "sa" // the lenght is less than 3 characters
        let promise = expectation(description: "Not fetch API")
        sut.getWeatherForeCast(city) { [weak self] _ in
            guard let strongSelf = self else { return }
            if strongSelf.sut.items.isEmpty {
                promise.fulfill()
            } else {
                XCTFail("Featched API")
            }
        }
        
        wait(for: [promise], timeout: 5)
    }
}
